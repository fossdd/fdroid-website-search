#! /bin/bash

#if [ -d "/tmpfs_fws" ] ; then
    cp -pr /opt/fdroid-website-search /tmpfs_fws/fdroid-website-search
    cd /tmpfs_fws/fdroid-website-search
#else
#    cd /opt/fdroid-website-search
#fi

/usr/local/bin/gunicorn fdroid_website_search.wsgi --bind 0.0.0.0:8000
